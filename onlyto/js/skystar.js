// poem
var words = [
    '伤心桥下春波绿',
    '曾是惊鸿照影来',
    '当年明月在',
    '曾照彩云归',
    '归去来兮',
    '真堪偕隐',
    '画船听雨眠',
    '愿为江水',
    '与君重逢',
    '一日不见兮',
    '思之若狂',
    '好想回到那天',
    '躲在后面偷偷看你',
    '你曾是我灰色人生中的一道彩虹',
    '柳絮空缱绻',
    '南风知不知',
    '我见青山多妩媚',
    '料青山见我也应如是',
    '取次花丛懒回顾',
    '半缘修道半缘君',
    '三笑徒然当一痴',
    '人生若只如初见',
    '我余光中都是你',
    '人生自是有情痴',
    '此恨不关风与月',
    '因为你，我多少适应了这个世界',
    '春蚕到死丝方尽',
    '蜡炬成灰泪始干',
    '今夜何夕',
    '见此良人',
    '愿我如星君如月',
    '夜夜流光相皎洁',
    '情不所起',
    '一往而深',
    '玲珑骰子安红豆',
    '入骨相思知不知',
    '多情只有春庭月',
    '尤为离人照落花',
    '若有知音见采',
    '不辞唱遍阳春',
    '休言半纸无多重',
    '万斛离愁尽耐担',
    '夜月一帘幽梦',
    '和光同尘',
    '杳霭流玉',
    '月落星沉',
    '霞姿月韵',
    '喜上眉梢',
    '醉后不知天在水',
    '满船星梦压星河',
    '落花人独立',
    '微雨燕双飞',
    '掬水月在手',
    '弄花香满衣',
    '夜深忽梦少年事',
    '唯梦闲人不梦君',
    '垆边人似月',
    '皓腕凝霜雪',
    '众里嫣然通一顾',
    '人间颜色如尘土',
    '若非群玉山头见',
    '会向瑶台月下逢',
    '沉鱼落雁鸟惊喧',
    '羞花闭月花愁颤',
    '解释春风无限恨',
    '沉香亭北倚阑干'
];
function randomNum(min, max) {
    var num = (Math.random() * (max - min + 1) + min).toFixed(2);
    return num;
}
function init() {
    let container = document.querySelector('.container');
    let f = document.createDocumentFragment();
    words.forEach(w => {
        let word_box = document.createElement('div');
        let word = document.createElement('div');
        word.innerText = w;
        word.classList.add('word');
        word.style.color = '#BAABDA';
        word.style.fontFamily = '楷体';
        word.style.fontSize = '20px'
        word_box.classList.add('word-box');
        word_box.style.setProperty("--margin-top", randomNum(-40, 20) + 'vh');
        word_box.style.setProperty("--margin-left", randomNum(6, 35) + 'vw');
        word_box.style.setProperty("--animation-duration", randomNum(8, 20) + 's');
        word_box.style.setProperty("--animation-delay", randomNum(-20, 0) + 's');

        word_box.appendChild(word);
        f.appendChild(word_box);


    })
    container.appendChild(f);
}
window.addEventListener('load', init);
let textone = document.querySelector('.textone').querySelector('h1');
let texttwo = document.querySelector('.texttwo').querySelector('h1');
let textthree = document.querySelector('.textthree').querySelector('h1');

setTimeout(function () {
    textone.innerHTML = '今晚，整片星空将为你一人闪烁';
    textone.style.color = '#E8F9FD';
    textone.style.fontFamily = '楷体'
    texttwo.style.color = '#E8F9FD';
    texttwo.style.fontFamily = '楷体'
    textthree.style.color = '#E8F9FD';
    textthree.style.fontFamily = '楷体'
    texttwo.innerHTML = '';
}, 5000)
setTimeout(function () {
    textone.innerHTML = '宝贝，希望我们就一直直这样走下去';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 10000)
setTimeout(function () {
    textone.innerHTML = '如蝴蝶奔向花海';
    texttwo.innerHTML = '如飞鸟向枝头，';
    textthree.innerHTML = '吹不散，彼此坚定';
}, 15000)
setTimeout(function () {
    textone.innerHTML = '春风太凌冽';
    texttwo.innerHTML = '烈日太娇横';
    textthree.innerHTML = '秋风仿佛有点悲伤';
}, 20000)
setTimeout(function () {
    textone.innerHTML = '冬雪仿佛又有些沉闷';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 25000)
setTimeout(function () {
    textone.innerHTML = '原来呀';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 30000)
setTimeout(function () {
    textone.innerHTML = '你是三餐之外的深夜甜酒';
    texttwo.innerHTML = '是四季之外无比舒适的第五个季节';
    textthree.innerHTML = '是我想携手共度一生的人';
}, 35000)
setTimeout(function () {
    textone.innerHTML = '若三餐四季你觉得乏味';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 40000)
setTimeout(function () {
    textone.innerHTML = '我愿给你我所能及的星河浪漫';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 45000)
setTimeout(function () {
    textone.innerHTML = '我喜欢每天不厌其烦地向你表达爱意';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 50000)
setTimeout(function () {
    textone.innerHTML = '尤其是在夜幕将尽之时';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 55000)
setTimeout(function () {
    textone.innerHTML = '想将这世间所有美好都统统给你';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 60000)
setTimeout(function () {
    textone.innerHTML = '祝宝贝做一个好梦';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 65000)
setTimeout(function () {
    textone.innerHTML = '梦里有我';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 70000)
setTimeout(function () {
    textone.innerHTML = '就让我替宇宙星河暂时照顾和保管你一段时间吧';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 75000)
setTimeout(function () {
    textone.innerHTML = '每一次见你';
    texttwo.innerHTML = '我一定会跑着去奔向你';
    textthree.innerHTML = '';
}, 80000)
setTimeout(function () {
    textone.innerHTML = '穿过时间的缝隙';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 85000)
setTimeout(function () {
    textone.innerHTML = '我偷偷向银河要了一把碎星';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 90000)
setTimeout(function () {
    textone.innerHTML = '只等你闭上眼睛';
    texttwo.innerHTML = '撒入你的梦中';
    textthree.innerHTML = '';
}, 95000)
setTimeout(function () {
    textone.innerHTML = '让你梦里有我，醒来拥我';
    texttwo.innerHTML = '';
    textthree.innerHTML = '';
}, 100000)
setTimeout(function () {
    textone.innerHTML = '宝贝雨宸，我爱你！';
    texttwo.innerHTML = ' ';
    textthree.innerHTML = '';
}, 105000)






